package co.haric.toyrobot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import co.haric.toyrobot.facing.v1.Facing;

/**
 * A pure unit test class for demonstrating of usage mocking, stubbing and verifying.
 */
@ExtendWith(MockitoExtension.class)
class RobotTest {
    @Mock
    private Board board;
    @Mock
    private Facing facingNorth;
    @Mock
    private Facing facingEast;

    private Robot robot;

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private static final PrintStream current = System.out;

    @BeforeEach
    void setUp() {
        robot = new Robot(board);
        System.setOut(new PrintStream(out));
    }

    @Test
    void rotate() {
        final Dimension dimension = new Dimension(0, 0);
        when(board.isValid(dimension)).thenReturn(true);
        robot.place(dimension, facingNorth);
        when(facingNorth.getRight()).thenReturn(facingEast);

        final Facing facing = robot.rotate(Robot.Rotation.RIGHT);
        assertEquals(facingEast, facing);
    }

    @Test
    void report() {
        final Dimension dimension = new Dimension(1, 2);
        when(board.isValid(dimension)).thenReturn(true);
        when(facingNorth.toString()).thenReturn("NORTH");
        robot.place(dimension, facingNorth);
        robot.report();
        final String report = out.toString();

        assertEquals("Output: 1,2,NORTH\n", report);
        verify(board, times(1)).isValid(any());
    }

    @Test
    void move() {
        final Dimension dimension = new Dimension(0, 0);
        when(board.isValid(any(Dimension.class))).thenReturn(true);
        when(facingNorth.getStep()).thenReturn(new Dimension(0,1));
        when(facingNorth.toString()).thenReturn("NORTH");

        robot.place(dimension, facingNorth);
        robot.move();
        robot.move();
        robot.report();

        String report = out.toString();

        assertEquals("Output: 0,2,NORTH\n", report);
        verify(board, times(3)).isValid(any());
        verify(facingNorth, times(2)).getStep();
    }

    @Test
    void moveBeforePlacement() {
        robot.move();
        robot.report();

        String report = out.toString();
        assertEquals("", report);
    }

    @Test
    void rotateBeforePlacement() {
        robot.rotate(Robot.Rotation.RIGHT);
        robot.report();

        String report = out.toString();
        assertEquals("", report);
    }

    @Test
    void placeWithInvalidCoordinateNotPlaced() {
        final Dimension dimension = new Dimension(-1, 0);
        when(board.isValid(dimension)).thenReturn(false);
        robot.place(dimension, facingNorth);
        robot.report();
        final String report = out.toString();

        assertEquals("", report);
    }

    @AfterEach
    void clean() {
        System.setOut(current);
    }
}