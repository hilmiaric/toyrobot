package co.haric.toyrobot;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


/**
 * This is an integration test without any mocking.
 * It simulates the inputs of the user.
 */
class RobotSimulatorIntegrationTest {

    private static final String RIGHT = "RIGHT\n";
    private static final String MOVE = "MOVE\n";
    private static final String REPORT = "REPORT\n";
    private static final String LEFT = "LEFT\n";
    private static final String QUIT = "q";
    private ByteArrayOutputStream out;
    private static final PrintStream prevSysOut = System.out;
    private static final InputStream prevSysIn = System.in;

    @BeforeEach
    void setUp() {
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    @AfterEach
    void tearDown() {
        System.setOut(prevSysOut);
        System.setIn(prevSysIn);
    }

    @Test
    void moveRobotWithMultiCommands() throws InterruptedException {
        Robot robot = new Robot(new Board());
        RobotSimulator simulator = new RobotSimulator(robot);
        Runnable sim = simulator::start;
        final Thread thread = new Thread(sim);
        thread.start();

        enterInput("PLACE 3,3,EAST\n" +
                REPORT +
                MOVE +
                LEFT +
                REPORT +
                MOVE +
                LEFT +
                REPORT +
                MOVE +
                LEFT +
                REPORT +
                MOVE +
                LEFT +
                REPORT +
                MOVE +
                RIGHT +
                REPORT +
                MOVE +
                "SOME RANDOM TEXT\n" +
                REPORT +
                QUIT);

        thread.join();

        String report = out.toString();

        Assertions.assertEquals("Toy Robot Simulator started\n" +
                "Please enter your next command:\n" +
                "Output: 3,3,EAST\n" +
                "Output: 4,3,NORTH\n" +
                "Output: 4,4,WEST\n" +
                "Output: 3,4,SOUTH\n" +
                "Output: 3,3,EAST\n" +
                "Output: 4,3,SOUTH\n" +
                "Output: 4,2,SOUTH\n", report);

    }

    @Test
    void rotateRobot360() throws InterruptedException {
        Robot robot = new Robot(new Board());
        RobotSimulator simulator = new RobotSimulator(robot);
        Runnable sim = simulator::start;
        final Thread thread = new Thread(sim);
        thread.start();

        enterInput("PLACE 2,2,EAST\n" +
                REPORT +
                LEFT +
                REPORT +
                LEFT +
                REPORT +
                LEFT +
                REPORT +
                LEFT +
                REPORT +
                RIGHT +
                REPORT +
                RIGHT +
                REPORT +
                RIGHT +
                REPORT +
                RIGHT +
                REPORT +
                QUIT);

        thread.join();

        String report = out.toString();

        Assertions.assertEquals("Toy Robot Simulator started\n" +
                "Please enter your next command:\n" +
                "Output: 2,2,EAST\n" +
                "Output: 2,2,NORTH\n" +
                "Output: 2,2,WEST\n" +
                "Output: 2,2,SOUTH\n" +
                "Output: 2,2,EAST\n" +
                "Output: 2,2,SOUTH\n" +
                "Output: 2,2,WEST\n" +
                "Output: 2,2,NORTH\n" +
                "Output: 2,2,EAST\n", report);

    }

    private void enterInput(String input) {
        input = input + "\n";
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }
}