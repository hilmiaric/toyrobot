package co.haric.toyrobot.facing.v1;

import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import co.haric.toyrobot.facing.Direction;

class FacingFactoryTest {

    @Test
    void getFaceNorth() {
        FacingFactory facingFactory = new FacingFactory();
        Assertions.assertEquals(Objects.requireNonNull(facingFactory.getFace(Direction.NORTH)).getClass(), North.class);
    }

    @Test
    void getFaceWest() {
        FacingFactory facingFactory = new FacingFactory();
        Assertions.assertEquals(Objects.requireNonNull(facingFactory.getFace(Direction.WEST)).getClass(), West.class);
    }

    @Test
    void getFaceSouth() {
        FacingFactory facingFactory = new FacingFactory();
        Assertions.assertEquals(Objects.requireNonNull(facingFactory.getFace(Direction.SOUTH)).getClass(), South.class);
    }

    @Test
    void getFaceEast() {
        FacingFactory facingFactory = new FacingFactory();
        Assertions.assertEquals(Objects.requireNonNull(facingFactory.getFace(Direction.EAST)).getClass(), East.class);
    }
}