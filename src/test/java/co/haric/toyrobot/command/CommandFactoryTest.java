package co.haric.toyrobot.command;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import co.haric.toyrobot.Robot;

class CommandFactoryTest {
    CommandFactory commandFactory = new CommandFactory();

    @Test
    void checkPlaceCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("PLACE 1,2,EAST".split(" "));
        assertTrue(command.isPresent());
    }

    @Test
    void checkInvalidCoordinateOnPlaceCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("PLACE 1,A,EAST".split(" "));
        assertFalse(command.isPresent());
    }

    @Test
    void checkInvalidFacingOnPlaceCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("PLACE 1,1,EA".split(" "));
        assertFalse(command.isPresent());
    }

    @Test
    void checkPlaceCommandWithMissingArgs() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("PLACE 1,2".split(" "));
        assertFalse(command.isPresent());
    }

    @Test
    void checkMoveCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("MOVE".split(" "));
        assertTrue(command.isPresent());
    }

    @Test
    void checkLeftCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("LEFT".split(" "));
        assertTrue(command.isPresent());
    }

    @Test
    void checkRightCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("RIGHT".split(" "));
        assertTrue(command.isPresent());
    }

    @Test
    void checkReportCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("REPORT".split(" "));
        assertTrue(command.isPresent());
    }

    @Test
    void checkInvalidCommand() {
        Optional<Consumer<Robot>> command = commandFactory.getCommand("SOME RANDOM TEXT".split(" "));
        assertFalse(command.isPresent());
    }
}