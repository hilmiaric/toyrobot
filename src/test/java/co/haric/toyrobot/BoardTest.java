package co.haric.toyrobot;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BoardTest {

    @Test
    public void checkValidTopRightCoordinate() {
        Board board = new Board(4,4);

        Dimension validCorDimension = new Dimension(4,4);
        boolean isValid = board.isValid(validCorDimension);
        Assertions.assertTrue(isValid);
    }

    @Test
    public void checkValidBottomLeftCoordinate() {
        Board board = new Board(4,4);

        Dimension validCorDimension = new Dimension(0,0);
        boolean isValid = board.isValid(validCorDimension);
        Assertions.assertTrue(isValid);
    }

    @Test
    public void inValidCoordinate() {
        Board board = new Board(4,4);

        Dimension validCorDimension = new Dimension(-1,0);
        boolean isValid = board.isValid(validCorDimension);
        Assertions.assertFalse(isValid, "Coordinate should be invalid");
    }

    @Test
    public void initWitInValidDimension() {
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> new Board(-1, 10));
    }

    @Test
    public void checkingNullCoordinate() {
        Board board = new Board();
        boolean isValid = board.isValid(null);
        Assertions.assertFalse(isValid, "Coordinate should be invalid");
    }
}