package co.haric;

import co.haric.toyrobot.Board;
import co.haric.toyrobot.Robot;
import co.haric.toyrobot.RobotSimulator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Board board = new Board();
        Robot robot = new Robot(board);
        RobotSimulator robotSimulator = new RobotSimulator(robot);
        robotSimulator.start();
    }
}
