package co.haric.toyrobot;

public class Dimension {
    private final int x;
    private final int y;

    public Dimension(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Dimension add(Dimension step) {
        return new Dimension(this.x + step.x,
                this.y + step.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    @Override
    public String toString() {
        return x + "," + y;
    }
}
