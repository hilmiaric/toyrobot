package co.haric.toyrobot;

import java.util.Scanner;

import org.jetbrains.annotations.NotNull;

import co.haric.toyrobot.command.CommandFactory;

public class RobotSimulator {
    private Robot robot;

    public RobotSimulator(@NotNull Robot robot) {
        this.robot = robot;
    }

    public void start() {
        final CommandFactory commandFactory = new CommandFactory();
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Toy Robot Simulator started");
            System.out.println("Please enter your next command:");
            while (true) {
                String input = scanner.nextLine();
                if ("q".equals(input)) {
                    break;
                }

                commandFactory.getCommand(input.split(" "))
                        .ifPresent(robotConsumer -> robotConsumer.accept(robot));
            }
        }
    }
}
