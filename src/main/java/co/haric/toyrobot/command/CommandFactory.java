package co.haric.toyrobot.command;

import static co.haric.toyrobot.command.CommandFactory.Commands.PLACE;

import java.util.EnumMap;
import java.util.Optional;
import java.util.function.Consumer;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.haric.toyrobot.Dimension;
import co.haric.toyrobot.Robot;
import co.haric.toyrobot.facing.Direction;
import co.haric.toyrobot.facing.v1.Facing;
import co.haric.toyrobot.facing.v1.FacingFactory;

public class CommandFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandFactory.class);

    private EnumMap<Commands, Consumer<Robot>> commandMap = null;

    public Optional<Consumer<Robot>> getCommand(String[] args) {

        if (PLACE.name().equals(args[0]) && args.length > 1) {
            return getPlaceCommandConsumer(args);
        }

        try {
            Commands command = Commands.valueOf(args[0]);
            return Optional.ofNullable(getCommandMap().get(command));
        } catch (IllegalArgumentException e) {
            LOGGER.error("", e);
        }

        return Optional.empty();

    }

    @NotNull
    private Optional<Consumer<Robot>> getPlaceCommandConsumer(String[] args) {
        final String[] position = args[1].split(",");
        if (position.length != 3) {
            return Optional.empty();
        }
        try {
            Dimension initDimension = new Dimension(Integer.parseInt(position[0]), Integer.parseInt(position[1]));
            final Facing face = new FacingFactory().getFace(Direction.valueOf(position[2]));
            if (face == null) {
                return Optional.empty();
            }
            return Optional.of(robot -> robot.place(initDimension, face));

        } catch (IllegalArgumentException e) {
            LOGGER.error("Invalid PLACE command {}", args, e);
        }
        return Optional.empty();
    }

    private EnumMap<Commands, Consumer<Robot>> getCommandMap() {
        if (commandMap == null) {
            initCommandMap();
        }
        return commandMap;
    }

    private void initCommandMap() {
        commandMap = new EnumMap<>(Commands.class);
        commandMap.put(Commands.MOVE, Robot::move);
        commandMap.put(Commands.LEFT, robot -> robot.rotate(Robot.Rotation.LEFT));
        commandMap.put(Commands.RIGHT, robot -> robot.rotate(Robot.Rotation.RIGHT));
        commandMap.put(Commands.REPORT, Robot::report);
    }

    enum Commands {
        PLACE,
        MOVE,
        LEFT,
        RIGHT,
        REPORT;
    }
}
