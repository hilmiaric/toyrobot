package co.haric.toyrobot;

public class Board {
    private final int dimensionX;
    private final int dimensionY;

    public Board() {
         this(5, 5);
    }

    public Board(int dimensionX, int dimensionY) {
        if (Math.min(dimensionX, dimensionY) < 1) {
            throw new IndexOutOfBoundsException("Dimensions should be greater than 0");
        }

        this.dimensionX = dimensionX;
        this.dimensionY = dimensionY;
    }

    public boolean isValid(Dimension dimension) {
        if (dimension == null) {
            return false;
        }

        return Math.min(dimension.getX(), dimensionX) == dimension.getX()
                && Math.min(dimension.getY(), dimensionY) == dimension.getY()
                && Math.min(dimension.getX(), dimension.getY()) >= 0;
    }
}
