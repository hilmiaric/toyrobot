package co.haric.toyrobot.facing;

public enum Direction {
        NORTH,
        SOUTH,
        EAST,
        WEST;
    }