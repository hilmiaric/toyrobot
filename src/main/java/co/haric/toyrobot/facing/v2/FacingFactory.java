package co.haric.toyrobot.facing.v2;

import static co.haric.toyrobot.facing.Direction.EAST;
import static co.haric.toyrobot.facing.Direction.NORTH;
import static co.haric.toyrobot.facing.Direction.SOUTH;
import static co.haric.toyrobot.facing.Direction.WEST;

import java.util.EnumMap;

import org.jetbrains.annotations.Nullable;

import co.haric.toyrobot.Dimension;
import co.haric.toyrobot.facing.Direction;

public class FacingFactory {
    private EnumMap<Direction, Facing> facingMap = null;

    @Nullable
    public Facing getDirection(Direction direction) {
        initFacingMap();
        return facingMap.get(direction);
    }

    private void initFacingMap() {
        if (facingMap == null) {
            facingMap = new EnumMap<>(Direction.class);
            Facing north = new Facing(NORTH, () -> new Dimension(0, 1));
            Facing south = new Facing(SOUTH, () -> new Dimension(0, -1));
            Facing east = new Facing(EAST, () -> new Dimension(1, 0));
            Facing west = new Facing(WEST, () -> new Dimension(-1, 0));
            north.setDirections(west, east);
            east.setDirections(north, south);
            south.setDirections(east, west);
            west.setDirections(south, north);

            facingMap.put(NORTH, north);
            facingMap.put(SOUTH, south);
            facingMap.put(EAST, east);
            facingMap.put(WEST, west);
        }
    }
}
