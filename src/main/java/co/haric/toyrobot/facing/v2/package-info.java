/**
 * This package contains an alternative facing implementation with a factory class that is based on enum map.
 * */
package co.haric.toyrobot.facing.v2;