package co.haric.toyrobot.facing.v2;

import java.util.function.Supplier;

import co.haric.toyrobot.Dimension;
import co.haric.toyrobot.facing.Direction;

public class Facing {
    private final Direction name;
    private Facing left;
    private Facing right;
    private final Supplier<Dimension> step;

    public Facing(Direction name, Supplier<Dimension> step) {
        this.name = name;
        this.step = step;
    }

    public void setDirections(Facing left, Facing right) {
        this.left = left;
        this.right = right;
    }

    public Facing getLeft() {
        return left;
    }

    public Facing getRight() {
        return right;
    }

    public Dimension getStep() {
        return step.get();
    }

    @Override
    public String toString() {
        return name.name();
    }
}
