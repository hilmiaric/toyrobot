package co.haric.toyrobot.facing.v1;

import org.jetbrains.annotations.Nullable;

import co.haric.toyrobot.facing.Direction;

public class FacingFactory {
    @Nullable
    public Facing getFace(Direction direction) {
        switch (direction) {
            case NORTH:
                return new North();
            case EAST:
                return new East();
            case SOUTH:
                return new South();
            case WEST:
                return new West();
            default:
                return null;
        }
    }
}
