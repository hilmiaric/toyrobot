package co.haric.toyrobot.facing.v1;

import co.haric.toyrobot.Dimension;

public class West extends Facing {
    public West() {
        super("WEST");
    }

    @Override
    public Facing getLeft() {
        return new South();
    }

    @Override
    public Facing getRight() {
        return new North();
    }

    @Override
    public Dimension getStep() {
        return new Dimension(-1,0);
    }
}

