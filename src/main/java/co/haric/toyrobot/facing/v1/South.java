package co.haric.toyrobot.facing.v1;

import co.haric.toyrobot.Dimension;

public class South extends Facing {
    public South() {
        super("SOUTH");
    }

    @Override
    public Facing getLeft() {
        return new East();
    }

    @Override
    public Facing getRight() {
        return new West();
    }

    @Override
    public Dimension getStep() {
        return new Dimension(0,-1);
    }
}


