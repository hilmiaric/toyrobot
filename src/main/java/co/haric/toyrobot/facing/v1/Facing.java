package co.haric.toyrobot.facing.v1;

import org.jetbrains.annotations.NotNull;

import co.haric.toyrobot.Dimension;

public abstract class Facing {
    private final String name;
    public Facing(@NotNull String name) {
        this.name = name;
    }

    public abstract Facing getLeft();

    public abstract Facing getRight();

    public abstract Dimension getStep();

    @Override
    public String toString() {
        return name;
    }
}
