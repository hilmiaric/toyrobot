package co.haric.toyrobot.facing.v1;

import co.haric.toyrobot.Dimension;

public class East extends Facing {
    public East() {
        super("EAST");
    }

    @Override
    public Facing getLeft() {
        return new North();
    }

    @Override
    public Facing getRight() {
        return new South();
    }

    @Override
    public Dimension getStep() {
        return new Dimension(1,0);
    }
}
