package co.haric.toyrobot.facing.v1;

import co.haric.toyrobot.Dimension;

public class North extends Facing {
    public North() {
        super("NORTH");
    }

    @Override
    public Facing getLeft() {
        return new West();
    }

    @Override
    public Facing getRight() {
        return new East();
    }

    @Override
    public Dimension getStep() {
        return new Dimension(0,1);
    }
}
