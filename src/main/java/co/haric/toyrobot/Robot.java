package co.haric.toyrobot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.haric.toyrobot.facing.v1.Facing;

public class Robot {
    private static final Logger LOGGER = LoggerFactory.getLogger(Robot.class);
    private static final String NOT_PLACED = "Robot not placed!";

    private Dimension dimension;
    private Facing facing;
    private Board board;

    public Robot(Board board) {
        this.board = board;
    }

    public Facing rotate(Rotation rotation) {
        if (isNotPlaced()){
            LOGGER.info(NOT_PLACED);
            return null;
        }

        if (rotation == Rotation.LEFT) {
            facing = facing.getLeft();
        } else if (rotation == Rotation.RIGHT) {
            facing = facing.getRight();
        }

        return facing;
    }

    public void report() {
        if (isNotPlaced()){
            LOGGER.info(NOT_PLACED);
            return;
        }

        System.out.println("Output: " + dimension.toString() + "," + facing.toString());
    }

    public void move() {
        if (isNotPlaced()){
            LOGGER.info(NOT_PLACED);
            return;
        }

        final Dimension newDimension = this.dimension.add(facing.getStep());
        if (board.isValid(newDimension)) {
            this.dimension = newDimension;
        }
    }

    public void place(Dimension dimension, Facing facing) {
        if (board.isValid(dimension) && facing != null) {
            LOGGER.info("Robot placed!");
            this.dimension = dimension;
            this.facing = facing;
        }
    }

    public enum Rotation {
        LEFT,
        RIGHT;
    }

    private boolean isNotPlaced() {
        return dimension == null;
    }
}
